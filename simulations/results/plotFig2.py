#!/usr/bin/python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Copyright (c) 2012 Vincent Gauthier.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------------

__author__ = """\n""".join(['Vincent Gauthier'])


def main(filedata):
    import csv
    import pylab as plt
    import numpy as np
    import matplotlib as mpl
    from scipy import interpolate
    mpl.rcParams['legend.numpoints'] = 1


    xnew = np.arange(0.2,1.5, 0.1)
    ynew = [0.0,5.0,10.0,60.0]
    X,Y = np.meshgrid(xnew,ynew)
    Z = []
    nb_files = 0
    for fileD in filedata:
        data_x = []
        data_y = []
        data_z = []
        
        with open(fileD, 'r') as f:
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in reader:
                data_x.append(float(row[0]))
                data_y.append(float(row[1]))
                data_z.append(float(row[2]))
            A = np.array(data_x)
            B = np.array(data_z)
            x = A/B
            z = np.array(data_y)
            f = interpolate.interp1d(x, z)
            znew = f(xnew)
            if nb_files == 0:
                Z = [znew]
            else:
                Z = np.vstack((Z, znew)) 
            nb_files += 1
    print Z

    plt.figure()
    plt.pcolor(X,Y,Z)
    plt.show()

if __name__ == '__main__':
    #filedata = ['./Data/data_2.dat', './Data/data_3_sort.dat', './Data/data_mobV2.dat', './Data/data_mobV60.dat']
    filedata = ['./Data/data_2.dat', './Data/data_mobV1.dat', './Data/data_mobV1.dat', './Data/data_mobV5.dat', './Data/data_mobV10.dat',]
    main(filedata)
