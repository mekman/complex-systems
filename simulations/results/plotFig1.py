#!/usr/bin/python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Copyright (c) 2012 Vincent Gauthier.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------------

__author__ = """\n""".join(['Vincent Gauthier'])


def main(filedata):
    import csv
    import pylab as plt
    import numpy as np
    import matplotlib as mpl
    from pylab import *
    import matplotlib as mpl
    from matplotlib.ticker import MaxNLocator
    mpl.rcParams['legend.numpoints'] = 1

    color = ['r--', 'b-o', 'm-s', 'g', 'y']

    fig = plt.figure()
    nb_file = 0
    plot_list = []
    for fileD in filedata:
        data_x = []
        data_y = []
        data_z = []
        
        with open(fileD, 'r') as f:
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in reader:
                data_x.append(float(row[0]))
                data_y.append(float(row[1]))
                data_z.append(float(row[2]))
        A = np.array(data_x)
        B = np.array(data_z)
        opt = color[nb_file]
        p, = plt.plot(A/B, data_y, opt, linewidth=2.0)
        plot_list.append(p)
    	nb_file += 1
    gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
    gca().set_xticks([
        0.2,
        0.4,
        0.6,
        0.8,
        1.0,
        1.2,
        1.4,
        ])
    xticklabels = getp(gca(), 'xticklabels')
    yticklabels = getp(gca(), 'yticklabels')
    setp(xticklabels, fontsize=11, weight='bold')
    setp(yticklabels, fontsize=11, weight='bold')
    plt.ylim([-0.03,1.1])
    plt.xlim([0.0,1.5])
    plt.legend(plot_list, ["Static", "Velocity = 5 m/s", "Velocity = 15 m/s" ], loc=2)
    plt.xlabel(r'Synergy $\eta \ (\eta = \frac{r}{\langle k \rangle})$')
    plt.ylabel(r'Fraction of cooperators')
    plt.show()
    fig.savefig('Figures/fig1.pdf', dpi=300)
    fig.savefig('Figures/fig1.eps', dpi=300)


if __name__ == '__main__':
    #filedata = ['./Data/data_2.dat', './Data/data_3_sort.dat', './Data/data_mobV2.dat', './Data/data_mobV60.dat']
    filedata = ['./Data/data_mobV0_1.dat', './Data/data_mobV5.dat', './Data/data_mobV15.dat']
    main(filedata)
