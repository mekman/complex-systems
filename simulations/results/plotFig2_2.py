#!/usr/bin/python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------------
# Copyright (c) 2012 Vincent Gauthier.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------------

__author__ = """\n""".join(['Vincent Gauthier'])


def main(filedata):
    import csv
    import numpy as np
    import pylab as plt
    from pylab import *
    import matplotlib as mpl
    from scipy import interpolate
    from matplotlib.image import NonUniformImage
    from matplotlib.ticker import MaxNLocator
    from matplotlib.pyplot import figure, show, pcolor, gca
    from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
    from mpl_toolkits.axes_grid1.inset_locator import mark_inset

    mpl.rcParams['legend.numpoints'] = 1

    yorig = [
        0.0,
        0.1,
        1.0,
        2.0,
        3.0,
        4.0,
        5.0,
        6.0,
        7.0,
        10.0,
        15.0,
        ]

    xnew = np.arange(0.0, 1.5, 0.1)
    ynew = np.arange(0, 16, 1)
    extent = (0.0, 1.5, 0, 16)

    (X, Y) = np.meshgrid(xnew, ynew)
    Z = []
    x = []
    y = []
    z = []
    nb_files = 0
    for fileD in filedata:
        data_x = []
        data_y = []
        data_z = []
        with open(fileD, 'r') as f:
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in reader:
                data_x.append(float(row[0]))
                data_y.append(float(row[1]))
                data_z.append(float(row[2]))
            A = np.array(data_x)
            B = np.array(data_z)
            x = np.append(x, A / B)
            y = np.append(y, [yorig[nb_files] for i in
                          range(len(data_x))])
            z = np.append(z, np.array(data_y))
            nb_files += 1
    f = interpolate.interp2d(x, y, z, kind='linear')
    znew = f(xnew, ynew)
    fig = figure()
    pcolor(X, Y, znew)

    fig = figure()

    interp = 'bilinear'
    ax = fig.add_subplot(111)
    im = NonUniformImage(ax, interpolation=interp, extent=extent)
    im.set_data(xnew, ynew, znew)
    ax.images.append(im)
    gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
    gca().set_xticks([
        0.2,
        0.4,
        0.6,
        0.8,
        1.0,
        1.2,
        1.4,
        ])
    xticklabels = getp(gca(), 'xticklabels')
    yticklabels = getp(gca(), 'yticklabels')
    setp(xticklabels, fontsize=11, weight='bold')
    setp(yticklabels, fontsize=11, weight='bold')
    fig.colorbar(im, ticks=[0, 0.5, 1])
    xlabel(r'Synergy $\eta \ (\eta = \frac{r}{\langle k \rangle})$')
    ylabel(r'Velocity (m/s)')


    axins = zoomed_inset_axes(ax, 1.5, loc=6)
    im = NonUniformImage(axins, interpolation=interp, extent=extent)
    im.set_data(xnew, ynew, znew)
    axins.images.append(im)

    axins.set_xlim(0.7, 1.1)
    axins.set_ylim(0.5, 5)
    plt.xticks(visible=False)
    plt.yticks(visible=False)

    mark_inset(
        ax,
        axins,
        loc1=3,
        loc2=1,
        fc='none',
        ec='0.1',
        )

    ax.set_xlim(0, 1.5)
    ax.set_ylim(0, 15)
    show()
    fig.savefig('Figures/fig2_2.pdf', dpi=300)
    fig.savefig('Figures/fig2_2.eps', dpi=300)


if __name__ == '__main__':

    filedata = [
        './Data/data_mobV0_1.dat',
        './Data/data_mobV1.dat',
        './Data/data_mobV2.dat',
        './Data/data_mobV3.dat',
        './Data/data_mobV4.dat',
        './Data/data_mobV5.dat',
        './Data/data_mobV6.dat',
        './Data/data_mobV7.dat',
        './Data/data_mobV8.dat',
        './Data/data_mobV10.dat',
        './Data/data_mobV15.dat',
        ]
    main(filedata)
