#!/usr/bin/python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------------
# Copyright (c) 2012 Vincent Gauthier.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------------

__author__ = """\n""".join(['Vincent Gauthier'])


def main(filedata):
    import csv
    import matplotlib.pyplot as plt
    import numpy as np
    import matplotlib as mpl
    import pickle
    from matplotlib.ticker import MaxNLocator
    from pylab import gca
    mpl.rcParams['legend.numpoints'] = 1

    # mpl.rcParams['legend.fontsize'] = 19

    letter = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        ]
    color = ['r--o', 'b-o', 'm-s', 'g-^', 'y--']
    velocity_list = [0, 1, 5, 10, 15]

    # fig = plt.figure()

    nb_file = 0
    plot_list = []
    fig = plt.figure(figsize=(12, 4))
    plt.subplots_adjust(hspace=.15, wspace=0.25)
    plotcounter = 1

    for fileD in filedata:

        # Open the file

        distribution = []
        X = []
        with open(fileD, 'r') as f:
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in reader:
                X.append(row[0])
                distribution.append(row[3])

        nbsubplot = str(len(filedata)) + str(len(distribution))
        distribution = distribution[0:4]
        nbL = len(filedata)
        nbC = len(distribution)
        subplot_row_index = 0
        for f in distribution:
            data = pickle.load(open(f, 'rb'))
            x = []
            y = []
            for (key, val) in data.iteritems():
                x.append(key)
                y.append(val)
            y_norm = np.array(y, dtype=float) / sum(y)
            print 'add plot', int(nbsubplot + str(plotcounter))
            ax = fig.add_subplot(nbL, nbC, plotcounter)

            # ax.text(10, .5, '(' +  letter[plotcounter-1] + ')' , fontweight='bold', fontsize=15)

            barplot = ax.bar(x, y_norm)

            plt.xlim([0, 101])
            plt.ylim([0, 0.05])

            if (plotcounter - 1) % len(distribution) == 0:
                plt.ylabel('Velocity = ' + str(velocity_list[nb_file]),
                           fontweight='bold', fontsize=12)

            if nb_file == 0:
                plt.title('R = ' + str(X[subplot_row_index]),
                          fontweight='bold', fontsize=12)

            if plotcounter > len(distribution) * len(filedata) / 2:
                gca().set_xticks([0.0, 100])
            else:
                ax.set_xticklabels(())

            if (plotcounter - 1) % len(distribution) == 0:
                gca().set_yticks([0.0, 0.05])
            else:
                ax.set_yticklabels(())

            plotcounter += 1
            subplot_row_index += 1
        nb_file += 1

    # plt.tight_layout()

    fig.savefig('Figures/fig4.pdf', dpi=300)
    fig.savefig('Figures/fig4.eps', dpi=300)
    plt.show()


if __name__ == '__main__':
    filedata = ['./DataFig34/datafile_Velocity01_ed.dat', 
                './DataFig34/datafile_Velocity1.dat',
                './DataFig34/datafile_Velocity5.dat',
                './DataFig34/datafile_Velocity10.dat', 
                './DataFig34/datafile_Velocity15S30.dat']
    main(filedata)
